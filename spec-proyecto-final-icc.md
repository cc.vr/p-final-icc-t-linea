## Descripción
Un par de comerciantes desean entrar al mundo del comercio electrónico, por esta razón
fuiste contratado para desarrollar una aplicación en Java que permita administrar y
consultar el inventario de la tienda en línea. A ellos les importa la seguridad de la
aplicación y la facilidad para consultar los productos que manejan. Algunos datos
relacionados a los productos son:
* Nombre del producto
* Peso del producto
* Dimensiones del producto (largo, alto y ancho)
* Color
* SKU
* Peso del envío (en gramos o kilogramos)
* La fecha desde la primer aparición del producto en la tienda
* Costo por unidad
* Etiquetas (cómputo, hogar y cocina, alimentos y bebidas, libros, etc.)

## Especificación
* El sistema permitirá consultar el catálogo de productos registrados a cualquier
 usuario no registrado y también a aquellos que ya lo estén.

* El sistema pedirá un usuario y contraseña para autenticar usuarios. Se espera
  que para dar de alta usuarios sea por medio de un archivo donde se especifique
  el rol que cada usuario tendrá y si los datos coinciden entonces permitirá acceder
  a la aplicación mostrando las operaciones que al usuario le corresponde dependiendo
  del rol que tenga.
  
* Los roles a considerar serán los siguientes:
  * Usuario sin registro (no autenticado)
  * Consumidor
  * Gerente
  * Administrador

* Las operaciones que se espera implementar son:  
  * Alta
  * Baja
  * Consulta

* Cuando sean incorrectos los datos de inicio de sesión se  notificará al usuario
  el número de intentos que tiene antes de bloquear el acceso para ese usuario
  por un tiempo determinado. Para esta implementación el número de intentos será de 3.

### Alta
* Esta operación solo será permitida por los usuarios que sean gerentes y administradores.
  Para dar de alta productos tendrá que recibirlos en una forma de una interfaz
  gráfica de usuario: 

* Se espera que cuando se termine de dar de alta un producto incremente en 1 el stock
  de la tienda.

### Baja
* Esta operación solo será permitda por un usuario que sea administrador.

* Se espera que la aplicación reciba el SKU de un producto a dar de bajar y un mensaje de
  confirmación para ejecutar o no la operación.
* Cuando un producto sea dado de baja, se espera que el stock del producto sea
  cero. Sin embargo, la información del producto se conservará y solo aparecerá
  una leyenda que diga `No disponible por el momento` para cuando el producto
  sea listado en la aplicación. 
* Al eliminar el producto tendrá que aparecer un mensaje notificando que el producto
  fue eliminado mostrando el nombre y el SKU.

## Consulta
* Las consultas podrán realizarse por cualquier usuario pero cuando un usuario
  esté registrado deberá mostrarse una leyenda en el producto que indique si el
  producto es elegible para envío gratis o no. En otro caso esta información no
  se despliega.
* Existirán dos tipos de consultas:
  * Aquella que obtiene todos los productos y su información
  * El resultado de una búsqueda especificando el SKU del producto.

## Autenticación
* Por ser una aplicación que maneja información delicada, tendrán que implementar el
  algoritmo de cifrado __Hill__ para almacenar y leer las contraseñas de los
  usuarios registrados con el fin de que estas no estén comprometidas dado que
  se espera guardar la información en un archivo de texto plano.

## Sobre dar de alta a los usuarios de la aplicación
Considera que para dar de alta a los usuarios con su rol y contraseña se espera
que los definas en un archivo de texto para ser "cargados" a la aplicación.

A continuación se sugiere el formato que el archivo podria llevar:
```bash
cecilia:administrador:c3c1l14
roberto:gerente:r0b3rt0
alejandra:consumidor:v0nMart1ny
```

**Nota:** Una vez leídos los usuarios, este archivo tendrá que actualizarse
reemplazando las contraseñas por su respectivo criptograma cifrado con una llave 
elegida por ustedes y espechficada en el archivo `asym.key`

## Sobre la persistencia de los productos
Considera que cada que existan productos dados de alta, la aplicación deberá
cargarlos con el fin de no perder información previamente procesada.

